package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/codeskyblue/go-sh"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var (
	RootDir = ""
)

type Sandbox struct {
	Id   string
	Code string
}

func mapRequestToDir(r *http.Request) string {
	workDir, _ := os.Getwd()
	clientId := r.RemoteAddr[:strings.Index(r.RemoteAddr, ":")]
	return workDir + "/tmp/" + clientId + "/"
}

// Echo the data received on the WebSocket.
func executionServer(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.RemoteAddr + " requested new simulation!")

	var dat map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&dat); err != nil {
		fmt.Println("JSON Parse Failed", err)
		w.Write([]byte("failed to parse the request"))
	}

	var id string
	if val, ok := dat["id"]; ok {
		id = val.(string)
		f, err := os.Create(mapRequestToDir(r) + id + ".cc")
		if err != nil {
			fmt.Println(err)
		}
		defer f.Close()
		if code, ok := dat["code"]; ok {
			f.Write([]byte(code.(string)))
		}
	}

	// Then Run the Code
	msg := Simulation(id)
	w.Write(msg)
}

func stopContainer(id string) error {
	session := sh.NewSession()
	_, err := session.
		Command("docker", "stop", id).
		CombinedOutput()
	if err != nil {
		fmt.Println("failed to stop docker", err)
		return err
	}

	_, err = session.
		Command("docker", "rm", id).
		CombinedOutput()

	if err != nil {
		fmt.Println("failed to rm docker", err)
		return err
	}
	return nil
}

// Echo the data received on the WebSocket.
func stopHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.RemoteAddr + " stopped, clean up!")

	var dat map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&dat); err != nil {
		fmt.Println("JSON Parse Failed", err)
		return
	}
	if val, ok := dat["id"]; ok {
		// remove the file
		if err := os.Remove(mapRequestToDir(r) + val.(string) + ".cc"); err != nil {
			fmt.Println(err)
		}
		stopContainer(val.(string))
	}
}

func Simulation(id string) []byte {
	fmt.Println("Running simulation for ", id)
	session := sh.NewSession()
	var msg []byte
	var err error

	// Run
	msg, err = session.
		Command("docker", "exec", id, "./waf", "--run", id).
		CombinedOutput()
	if err != nil {
		// if failed, return err
		fmt.Println(err)
		return msg
	}

	fmt.Println("Simulation for ", id, " done")
	return msg
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	// Create the folder as r.RemoteAddr
	cid := createContainer(r)
	w.Write([]byte(cid))
}

func createContainer(r *http.Request) string {
	// TODO(benzh) Check mkdir
	_ = os.Mkdir(mapRequestToDir(r), 0777)
	fmt.Println("Container created at " + mapRequestToDir(r))

	// Create Docker container here
	session := sh.NewSession()
	// Start the container
	cid, err := session.
		Command("docker", "run", "-d", "-v", mapRequestToDir(r)+":/workspace/bake/source/ns-3.22/scratch", "-it", "ns3").
		CombinedOutput()
	if err != nil {
		fmt.Println(err)
	}

	id := string(cid[:8])
	fmt.Println("started container ", id)
	return id
}

// Everytime a connection is estabilshed, respond with the template index.html
// inserted with sample file.
func rootHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("public/index.html")
	if err != nil {
		log.Fatal(err)
	}

	code, err := ioutil.ReadFile("public/sample/0.cc")
	if err != nil {
		log.Fatal(err)
	}

	s := &Sandbox{
		Id:   "xxxxxxx",
		Code: string(code),
	}

	t.Execute(w, s)
}

func sampleHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(RootDir + "/public/" + r.URL.Path[1:])
	http.ServeFile(w, r, RootDir+"/public/"+r.URL.Path[1:])
}

func myfileHandler(w http.ResponseWriter, r *http.Request) {
	http.StripPrefix("/myfile/",
		http.FileServer(http.Dir(mapRequestToDir(r)))).ServeHTTP(w, r)
}

func main() {
	flag.Parse()

	var err error
	RootDir, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Playground Server")
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/exec/", executionServer)
	http.HandleFunc("/start/", startHandler)
	http.HandleFunc("/stop/", stopHandler)
	http.HandleFunc("/sample/", sampleHandler)
	http.HandleFunc("/myfile/", myfileHandler)

	http.Handle("/public/", http.StripPrefix("/public/",
		http.FileServer(http.Dir("public"))))

	log.Fatal(http.ListenAndServe(":12345", nil))
}
