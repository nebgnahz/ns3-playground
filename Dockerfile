FROM ubuntu:latest
MAINTAINER benzh < benzh [at] cs [dot] berkeley [dot] edu >

RUN apt-get update
RUN apt-get -y install mercurial
RUN apt-get -y install gcc
RUN apt-get -y install g++
RUN apt-get -y install python

RUN mkdir -p /workspace && cd /workspace && hg clone http://code.nsnam.org/bake
WORKDIR /workspace/bake
RUN ./bake.py configure -e ns-3.22
RUN ./bake.py check
RUN ./bake.py download
RUN ./bake.py build -vvv
RUN cd /workspace/bake/source/ns-3.22 && ./test.py -c core
RUN cd /workspace/bake/source/ns-3.22 && ./waf --run hello-simulator

WORKDIR /workspace/bake/source/ns-3.22