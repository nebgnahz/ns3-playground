$("#run").click(function() {
    // serialize the content
    var id = $("#id").text();
    var code = editor.getSession().getValue();
    var data = {id: id, code: code};

    $("#run").addClass("disabled");
    $("#loadingDiv").append("<button id=\"loading\" class=\"btn btn-sm btn-warning right\"><span class=\"glyphicon glyphicon-refresh glyphicon-refresh-animate\"></span>Loading...</button>")

    $.ajax({
        type: "POST",
        url: "/exec/",
        data: JSON.stringify(data),
    }).done(function(data) {
        result = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
        $("#result").remove()
        $("#loading").remove()
        $("#output").append("<p id=\"result\">" + result + "</p>")
        $("#run").removeClass("disabled");
    });
});

$(window).load(function() {
    var id = $("#id").text();
    var data = {id: id}
    $.ajax({
        type: "POST",
        url: "/start/",
        data: JSON.stringify(data),
    }).done(function(data) {
        $("#id").text(data)
    });
});

window.onbeforeunload = function() {
    var id = $("#id").text();
    var data = {id: id}
    $.ajax({
        type: "POST",
        url: "/stop/",
        data: JSON.stringify(data),
    });
    alert("closing")
};

$(".sample").click(function() {
    // load $(this).index()
    $.ajax({
        type: "GET",
        url: "/sample/" + $(this).index() + ".cc"
    }).done(function(data) {
        editor.getSession().setValue(data)
    });
});
