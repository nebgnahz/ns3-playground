// Omitted GPLv2 License for readability. Read more at:
// https://www.nsnam.org/developers/contributing-code/licensing/.

#include "ns3/core-module.h"

NS_LOG_COMPONENT_DEFINE ("HelloSimulator");

using namespace ns3;

int
main (int argc, char *argv[])
{
  NS_LOG_UNCOND ("Hello Simulator");
}
