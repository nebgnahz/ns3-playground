// Omitted GPLv2 License for readability. Read more at:
// https://www.nsnam.org/developers/contributing-code/licensing/.
//
// Model:
//   +--------------+                                  +--------------+
//   |  Appliction  |  packet                      +-> |  Appliction  |
//   +--------------+    |                         |   +--------------+
//   +--------------+    |                         |   +--------------+
//   |              |    |                         |   |              |
//   |   Protocol   |    |                         |   |   Protocol   |
//   |    Stack     |    |                         |   |    Stack     |
//   |              |    +-------------------------+   |              |
//   +--------------+                                  +--------------+
//   +--------------+          +-------------+         +--------------+
//   |   NetDevice  |<========>|   Channel   |<=======>|   NetDevice  |
//   +--------------+          +-------------+         +--------------+
//
// --------------------                              ---------------------
//       Node 1                                             Node 2

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("FirstScriptExample");

int
main (int argc, char *argv[])
{
  Time::SetResolution (Time::NS);
  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  // Create two nodes to hold.
  NodeContainer nodes;
  nodes.Create (2);

  // Channel: PointToPoint, a direct link with `DataRate` and `Delay` specified.
  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  // NetDevice: installed onto the channel
  NetDeviceContainer devices;
  devices = pointToPoint.Install (nodes);

  // Protocol Stack: Internet Stack
  InternetStackHelper stack;
  stack.Install (nodes);

  // Since IP Address assignment is so common, the helper does the dirty work!
  // You only need to set the base.
  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  // Assign the address to devices we created above
  Ipv4InterfaceContainer interfaces = address.Assign (devices);

  // Application layer: UDP Echo Server and Client
  // 1, Server:
  UdpEchoServerHelper echoServer (9);
  ApplicationContainer serverApps = echoServer.Install (nodes.Get (1));
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));

  // 2, Client:
  UdpEchoClientHelper echoClient (interfaces.GetAddress (1), 9);
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024));
  ApplicationContainer clientApps = echoClient.Install (nodes.Get (0));
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (10.0));

  // Start Simulation
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
