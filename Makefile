BIN:
	go build -o playground playground.go

run:
	./playground
clean:
	rm -rf playground

docker:
	@mkdir -p tmp
	sudo docker build -t ns3 .

.PHONY: BIN clean docker
