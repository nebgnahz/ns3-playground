# README #

This repository hosts files needed to setup a playground for ns-3 simulator.

### Get Started ###

* Clone this repository: `git clone https://nebgnahz@bitbucket.org/nebgnahz/ns3-playground.git`
* Install Docker
* `make docker`
* `make`
* `make run`